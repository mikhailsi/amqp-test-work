<?php
$loader = require_once __DIR__.'/vendor/autoload.php';
$loader->add('TestWork', __DIR__ . '/../TestWork/');

use \TestWork\Application;
use \TestWork\Exception;

try {
    (new Application(include __DIR__ . '/TestWork/config.php'))->start();
} catch (Exception $e) {
    echo $e->getMessage();
}
