<?php
namespace TestWork;

use PhpAmqpLib\Channel\AMQPChannel;

class Receiver
{
    protected $_db;
    protected $_channel;
    protected $_channelName;
    protected $_statement;

    public function __construct(AMQPChannel $channel, \PDO $db, $channelName)
    {
        $this->_channel = $channel;
        $this->_db = $db;
        $this->_channelName = $channelName;
        $this->waitForMessages();
    }

    public function waitForMessages()
    {
        echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";
        $this->_channel->basic_consume(
            $this->_channelName,
            '',
            false,
            true,
            false,
            false,
            [$this, 'putMessageToDB']
        );

        while(count($this->_channel->callbacks)) {
            $this->_channel->wait();
        }
    }

    public function putMessageToDB($message)
    {
        try {
            $body = json_decode($message->body);
        } catch (\Exception $e) {
            throw new Exception('invalid json: ' . $message->body);
        }

        $statement = $this->getStatement();
        echo "received -> $message->body\n";

        if ($statement->execute([
            'name' => $body->name,
            'value' => $body->value * 10000000,
            'time' => $body->time,
        ])) {
            echo 'message saved to DB' . PHP_EOL;
        } else {
            echo 'can\'t save message to DB' . PHP_EOL;
        }
    }

    protected function getStatement()
    {
        return $this->_statement ?: ($this->_statement = $this->_db->prepare(
            'INSERT INTO `message` (`name`, `value`, `time`) VALUES (:name, :value, :time)'
        ));
    }
}
