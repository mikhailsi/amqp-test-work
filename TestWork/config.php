<?php
return [
    'factory' => [
        'class' => '\TestWork\Factory',
        'construct' => [
            'application' => true
        ],
    ],
    'db' => [
        'class' => '\PDO',
        'construct' => [
            'dsn' => 'mysql:host=localhost;dbname=messages',
            'username' => 'test',
            'password' => 'test',
        ],
    ],
    'rabbit' => [
        'class' => '\PhpAmqpLib\Connection\AMQPStreamConnection',
        'construct' => [
            'host' => 'localhost', 
            'port' => 5672, 
            'login' => 'guest', 
            'password' => 'guest',
        ],
    ],
];
