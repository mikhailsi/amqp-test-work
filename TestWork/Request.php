<?php

namespace TestWork;

class Request
{
    protected $_count;
    protected $_role;
    protected $_channel;

    public function __construct()
    {
        switch (true) {
            case $this->isCLI():
                $params = getopt('', ['role:', 'channel:', 'count']);
                break;
            default:
                $params = $_GET;
        }

        $this->_count = $params['count'] ?? null;
        $this->_role = $params['role'] ?? null;
        $this->_channel = $params['channel'] ?? null;
    }

    public function getRole()
    {
        return $this->_role;
    }

    public function getChannel()
    {
        return $this->_channel;
    }

    public function getCount()
    {
        return $this->_count;
    }

    public function isCLI()
    {
        return php_sapi_name() === 'cli';
    }
}