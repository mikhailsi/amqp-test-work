<?php

namespace TestWork;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;

class Sender
{
    public function __construct(AMQPChannel $channel, $channelName = '')
    {
        return $this->generateMessage($channel, $channelName);
    }

    protected function generateMessage($channel, $channelName)
    {
        $msg = new AMQPMessage(json_encode($this->getMessage()));
        return $channel->basic_publish($msg, '', $channelName);
    }

    public function getMessage()
    {
        $currencies = [
            'EUR/USD',
            'EUR/GBP',
            'GBP/USD'
        ];

        return [
            'name' => $currencies[mt_rand(0, count($currencies) - 1)],
            'value' => mt_rand(10000000, 20000000)/ 10000000,
            'time' => time(),
        ];
    }
}
