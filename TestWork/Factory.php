<?php
namespace TestWork;

class Factory
{
    protected $_application;
    
    public function __construct(Application $application)
    {
        $this->_application = $application;
    }

    public function getChannel($channelName)
    {
        $connection = $this->_application->rabbit;
        $channel = $connection->channel();
        $channel->queue_declare($channelName, false, false, false, false);
        return $channel;
    }

    public function getSender($channelName)
    {
        return new Sender($this->getChannel($channelName), $channelName);
    }
    
    public function getReceiver($channelName)
    {
        return new Receiver($this->getChannel($channelName), $this->_application->db, $channelName);
    }
}
