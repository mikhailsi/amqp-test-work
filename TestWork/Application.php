<?php
namespace TestWork;

/**
 * @property $factory \TestWork\Factory
 * @property $rabbit \PhpAmqpLib\Connection\AMQPStreamConnection
 */

class Application
{
    const ROLE_RECEIVER = 'receiver';
    const ROLE_SENDER = 'sender';
    
    protected $_request;
    protected $_config = [];
    protected $_components = [];
    
    public function __construct(array $config)
    {
        $this->_config = $config;
        $this->_request = new Request;
    }

    public function start()
    {
        switch ($this->_request->getRole()) {
            case static::ROLE_SENDER:
                for ($i = 0; $i < $this->_request->getCount(); $i++) {
                    $this->factory->getSender($this->_request->getChannel());
                }

                echo $this->_request->getCount() . ' messages sent' . PHP_EOL;
                break;
            case static::ROLE_RECEIVER:
                return $this->factory->getReceiver($this->_request->getChannel());
            default:
                throw new Exception('wrong role use "receiver" or "sender"' . PHP_EOL);
        }
    }

    public function __get($name)
    {
        switch (true) {
            case !array_key_exists($name, $this->_config):
                throw new Exception('cant create component ' . $name . PHP_EOL);
            case !array_key_exists($name, $this->_components):
                $class = $this->_config[$name]['class'];

                $params = isset($this->_config[$name]['construct'])
                    && is_array($this->_config[$name]['construct'])
                    ? $this->_config[$name]['construct']
                    : [];

                array_key_exists('application', $params)
                    ? ($params['application'] = $this)
                    : null;

                return $this->_components[$name] = new $class(
                    ...array_values($params)
                );
            case array_key_exists($name, $this->_components):
                return $this->_components[$name];
        }
    }
}
