В качестве шины данных используется RabbitMQ

Для запуска модуля отправки сообщений используется команда:
`php init.php --count=10 --channel=first --role=sender`

Для запуска модуля приема сообщений используется команда:
`php init.php --role=receiver --channel=first`
